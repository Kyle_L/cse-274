## CSE 274
This contains all homework projects from CSE 274 at Miami University during the 2018 - 2019 school year.

## Code style
All code is formatted according the requirements of the class and its professors. 

## Projects Included
Includes Homework projects... 

* Homework 1 PalBook  
	* Filename: HW1-PalBook  
*  Homework 2 Set  
	*  Filename: HW2-Set  
*  Homework 3 SortedLinkedList
	*  Filename: HW3-SortedLinkedList
*  Homework 4 Stack
	*  Filename: HW4-Stack
*  Homework 5 Queues
	*  Filename: HW5-Queues
* Homework 6 LinkedListWithTail
	*  Filename: HW6-LinkedListWithTail
* Homework 7 Dictionaries
	*  Filename: HW7-Dictionaries
* Homework 8 SpeedyLookUp
	*	Filename: HW8-SpeedyLookUp
	
## Directory Format
Each homeworks assignment is formatted in the following manner...

* HW[Number of homework assignment]-[Name of homework assignment] : General homework directory.
	* Project[Number of homework assignment] : Where the project itself is.
	* Resources : Where the instructor given material and instructions are.
	
## License
Copyright Kyle Lierer (2018)