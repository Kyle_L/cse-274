///////////////////////////////////////////////////////////////////////////////
//
// Title:            ResizableArraySet
// This File:        ResizableArraySet.java
// Files:            ResizableArraySet.java, SetTester.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (ResizableArraySet.java) have been thoroughly tested and
//                   should perform the implemented operations.
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Arrays;

/**
 * A collection that contains no duplicate elements.
 * @author Kyle Lierer
 * @param <T> the type of elements maintained by this set
 */
public class ResizableArraySet<T> implements SetInterface<T> {

	private int mySize;
	private T myArray[];
	private final static int DEFAULT_SIZE = 5;

	public ResizableArraySet () {
		this(DEFAULT_SIZE);
	}

	@SuppressWarnings("unchecked")
	public ResizableArraySet (int size) {

		mySize = 0;
		myArray = (T[])new Object[size];
	}

	/**
	 * Returns the number of elements in the set.
	 * @return the number of elements in the set.
	 */
	@Override
	public int getSize() {
		return mySize;
	}

	/**
	 * Returns whether the set is empty or not.
	 * @return whether the set is empty or not.
	 */
	@Override
	public boolean isEmpty() {
		return mySize == 0;
	}

	/**
	 * Adds an element to the set if it already not present.
	 * @param newEntry the element to be added as a new entry.
	 * @return whether the element was added successfully as a new entry or not.
	 */
	@Override
	public boolean add(T newEntry) {
		//If it already contains newEntry, don't add that.
		if (contains(newEntry)) return false;

		//Increase the size of the array if it is too small.
		if (mySize == myArray.length) {
			myArray = Arrays.copyOf(myArray, myArray.length * 2);
		}

		//Add the entry and increase the size.
		myArray[mySize] = newEntry;
		mySize++;
		
		return true;
	}

	/**
	 * Removes a specified entry from the set.
	 * @param anEntry the entry to be removed.
	 * @return whether the entry was removed or not.
	 */
	@Override
	public boolean remove(T anEntry) {
		//Finds the entry in the array.
		for(int i = 0; i < mySize; i++) {
			if (myArray[i].equals(anEntry)) {	
				
				/*Shifts all points back one in the array
				to remove the found Point.*/
				for (int j = i; j < mySize - 1; j++) {
					myArray[j] = myArray[j + 1];
				}
				
				//Sets the last element null since it was shifted.
				myArray[mySize - 1] = null;

				//Increases size.
				mySize--;
				
				//Decrease the array size if size is less than half the size of all entries.
				if (mySize < myArray.length/2) {
					myArray = Arrays.copyOf(myArray, myArray.length / 2);
				}
				return true;	
			}
		}
		return false;
	}

	/**
	 * Removes an unspecified entry from the set.
	 * @return the entry that was removed from the set.
	 */
	@Override
	public T remove() {
		//Pre-condition.
		if (isEmpty()) return null;
		
		//Removes the Point based on the current size of the bag.
		T p = myArray[mySize - 1];
		myArray[mySize - 1] = null;
		
		//Decreases size.
		mySize--;

		//Decrease the array size if size is less than half the size of all entries.
		if (mySize < myArray.length << 1) {
			myArray = Arrays.copyOf(myArray, myArray.length << 1);
		}
		return p;
	}

	/**
	 * Removes all entries from the set.
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		mySize = 0;
		myArray = (T[])new Object[1];
	}

	/**
	 * Checks whether an entry is present in the set.
	 * @param anEntry The entry that is being checked.
	 * @return whether is entry is present in the set or not.
	 */
	@Override
	public boolean contains(T anEntry) {
		/*Traverses the array and finds if there is an
		object in the set that is equal to anEntry.*/
		for(int i = 0; i < mySize; i++) {
			if (myArray[i].equals(anEntry)) {
				return true;	
			}
		}
		return false;
	}

	/**
	 * Adds all of the entries in the specified set to this set if they're not already present and returns the result.
	 * @param anotherSet another set.
	 * @return the result of both sets being added together.
	 */
	@Override
	public SetInterface<T> union(SetInterface<T> anotherSet) {
		//Create a temporary array.
		T tempArray[] = anotherSet.toArray();

		ResizableArraySet<T> result = new ResizableArraySet<T>();

		//Iterate through the array and add everything.
		for (int i = 0; i < this.getSize(); i++) {
			result.add(myArray[i]);
		}

		//Iterate through temp array and add everything (doesn't add dups because its a set).
		for (int i = 0; i < anotherSet.getSize(); i++) {
			result.add(tempArray[i]);
		}

		return result;
	}

	/**
	 * Retains only the entries in this set that are contained in the specified set and returns the result.
	 * @param anotherSet another set
	 * @return the entries that contained in both this set and the specified.
	 */
	@Override
	public SetInterface<T> intersection(SetInterface<T> anotherSet) {
		//Create a temporary array.
		Object tempArray[] = anotherSet.toArray();

		ResizableArraySet<T> result = new ResizableArraySet<T>();

		//Iterates through the array and finds elements in tempArray that are in this array.
		for (int i = 0; i < this.getSize(); i++) {
			for (int j = 0; j < anotherSet.getSize(); j++) {
				if (myArray[i].equals(tempArray[j])) result.add(myArray[i]);
			}
		}

		return result;
	}

	/**
	 * Converts the set to an array of type T.
	 * @return the array.
	 */
	@Override
	public T[] toArray() {
		return Arrays.copyOf(myArray, mySize);
	}

	/**
	 * Returns the String representation of the set.
	 * @return the String representation of the set.
	 */
	@Override
	public String toString () {
		return Arrays.toString(this.toArray());
	}

}
