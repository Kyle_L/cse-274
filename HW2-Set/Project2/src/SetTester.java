///////////////////////////////////////////////////////////////////////////////
//
// Title:            Tester
// This File:        Tester.java
// Files:            ResizableArraySet.java, SetTester.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (SetTester.java) have been thoroughly tested and
//                   should perform the tests on ResizableArraySet.
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Tests the implementation of SetInterface, ResizableArraySet
 * @author Kyle
 *
 */
public class SetTester {

	public static void main(String[] args) {

		//Testing adding.
		System.out.println("**Testing adding**");
		ResizableArraySet<Integer> arrayA = new ResizableArraySet<>();

		for (int i = 1; i <= 15; i++ ) {
			System.out.println("Adding " + i + ". true? " + arrayA.add(i));
		}
		//Adding when full.
		System.out.println("Adding " + 15 + ". false? " + arrayA.add(15));

		//Status update.
		System.out.println();
		System.out.println("The set is currently: " + arrayA);
		System.out.println("The set size is currently: " + arrayA.getSize());
		System.out.println();

		//Testing contains
		System.out.println("**Testing contains**");
		System.out.println("Does the set contain 15? Expected true: " + arrayA.contains(15));
		System.out.println("Does the set contain 25? Expected false: " + arrayA.contains(25));
		System.out.println();


		//Testing removing.
		System.out.println("**Testing remove**");

		for (int i = 1; i <= 15; i+=2 ) {
			System.out.println("Removing " + i + ". true? " + arrayA.remove(i));
		}

		//Removing when already removed.
		System.out.println("Removing " + 15 + ". false? " + arrayA.remove(15));

		//Status update
		System.out.println();
		System.out.println("The set is currently: " + arrayA);
		System.out.println("The set size is currently: " + arrayA.getSize());
		System.out.println();

		//Emptying set.
		System.out.println("**Testing removing everything**");

		while (!arrayA.isEmpty()) {
			System.out.println(arrayA.remove().toString() + " removed from the set.");
		}

		//Status update.
		System.out.println();
		System.out.println("The set is currently: " + arrayA);
		System.out.println("The set size is currently: " + arrayA.getSize());
		System.out.println();

		//Testing intersection and union.
		System.out.println("**Testing intersection and union**");

		System.out.println("Creating and filling two new sets (A & B).");

		ResizableArraySet<Integer> array1 = new ResizableArraySet<>();
		ResizableArraySet<Integer> array2 = new ResizableArraySet<>();

		for (int i = 1; i <= 10; i+=3 ) {
			array1.add(i);
		}

		System.out.println("Set A's state" + array1.toString());
		
		for (int i = 0; i <= 10; i+=2 ) {
			array2.add(i);
		}

		System.out.println("Set B's state" + array2.toString());
		System.out.println();

		System.out.println("Union: " + array1.union(array2).toString());
		System.out.println("Intersection: " + array1.intersection(array2).toString());
		System.out.println();

		//Testing clear.
		System.out.println("**Testing clear**");
		System.out.println("Both sets cleared.");
		array1.clear();
		array2.clear();
		System.out.println("Set A's state" + array1.toString());
		System.out.println("Set B's state" + array2.toString());

	}

}
