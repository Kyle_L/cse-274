///////////////////////////////////////////////////////////////////////////////
//
// Title:            Infix Calculator
// This File:        InfixExpression.java
// Files:            ArrayStack.java, InfixExpression.java, Project4Tester.java, StackInterface.java, Tester.java.
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code have been thoroughly tested and
//                   should perform the operations listed in Project4 - Expression Calculator.docx. Please note, this code is dependent on Pal.java.
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * A class that stores a valid infix expression and can evaluate it.
 * @author Kyle Lierer
 *
 */
public class InfixExpression {

	private String infix;
	
	public InfixExpression (String expression) {
		//Sets the infix to the expression.
		infix = expression;

		//Cleans the expression.
		clean();

		//Checks if the expression is valid.
		if (!isValid()) 
			throw new IllegalArgumentException();		
	}
	
	/**
	 * Returns the cleaned up infix expression as a string.
	 * @return the cleaned up infix expression.
	 */
	@Override
	public String toString () {
		return infix;
	}
	
	/**
	 * Returns whether an a infix expression is balanced.
	 * @return whether the expression is balanced or not.
	 */
	private boolean isBalanced () {
		//The stack which parenthesis are pushed onto.
		StackInterface<Character> stack = new ArrayStack<>();
		
		//Iterates through the entire stack.
		for (int i = 0; i < infix.length(); i++) {
			
			//If it is an opening parenthesis, push it on the stack.
			if (infix.charAt(i) == '(')
				stack.push(infix.charAt(i));
			
			//If it is a closing parenthesis, pop it.
			else if (infix.charAt(i) == ')') {
				if (stack.isEmpty()) return false;
				
				char top = stack.pop();
			
				//If the popped parenthesis matches the opening parenthesis.
				if (infix.charAt(i) == ')' && top != '(') {
					return false;
				}
				
			}
		}

		//If the stack is not empty, the expression is not balanced.
		if (stack.isEmpty()) return true;
		return false;
	}
	
	/**
	 * Checks if the infix expression can be evaluated.
	 * @return whether the infix expression can be evaluated or not.
	 */
	private boolean isValid () {
		if (!isBalanced()) return false;
			
		//Contains the clean special character "_".
		if(infix.contains("_")) return false;
		
		String[] str = infix.split(" ");
		
		int openParenthCount = 0;
		int operatorCount = 0;
		int operandCount = 0;
		int operandCountInside = 0;
	    boolean lastWasOp = false;
	    boolean lastWasOpen = false;

	    //Iterates through the string.
	    for (String c : str) {
	        if(c == " ") continue;
	        
	        //On opening bracket.
	        if (c.equals("(")) {
	        	operandCountInside++;
	            openParenthCount++;
	            lastWasOpen = true;
	            continue;
	            
            //On closing bracket.
	        } else if (c.equals(")")) {
	            if (openParenthCount <= 0 || lastWasOp || operandCountInside < 2) {
	            	operandCountInside = 0;
	                return false;
	            }
	            openParenthCount--;
	            
            //On operator.
	        }else if (getOperatorRank(c) > 0){
	        	operatorCount++;
	        	operandCountInside++;
	            if (lastWasOp || lastWasOpen) return false;
	            lastWasOp = true;
	            continue;
	            
            //On not int.
	        }else if(!isAnInt(c)){
	            return false;
	            
            //On int.
	        } else if (isAnInt(c)) {
		        operandCount++;
	            operandCountInside++;
	        }
	        lastWasOp = false;
	        lastWasOpen = false;
	    }
	    if(openParenthCount != 0) return false;
	    if(lastWasOp || lastWasOpen) return false;
	    if (operatorCount != operandCount - 1) return false;

		return true;
	}
	
	/**
	 * Cleans the the infix expression. Makes an expression like"5+5" look like "5 + 5".
	 */
	private void clean () {
		//Finds spaces between numbers and replace it with "_".
		infix = infix.replaceAll("(?<=\\d) +(?=\\d+\\b)", "_");
		
		//Removes all spaces.
		infix = infix.replaceAll("\\s", "");
		
		//Add spaces between every character.
		infix = infix.replaceAll("", " ").trim();
		
		//Remove all spaces except between numbers.
		infix = infix.replaceAll("(?<=\\d) +(?=\\d)", "");
		
		//Replace "_" with a space. This keeps the space between two different numbers.
		infix = infix.replaceAll("_", "");
	}
	
	/**
	 * Checks if a string can be converted to an int.
	 * @param aString The string that is being check if it can be converted.
	 * @return whether the string is a int or not.
	 */
	private boolean isAnInt (String aString) {
		/*Tries to convert a string to an int.
		if it fails, it is not an int.*/
		try {
			Integer.parseInt(aString);
		} catch (Exception e) {
			return false;
		}
		return true;
	}
	
	/**
	 * Returns the rank of an operator.
	 * @param aOperator the operator thats rank is being determined.
	 * @return the rank of the operator.
	 */
	private int getOperatorRank (String aOperator) {
		switch (aOperator) {
		case "+": case "-":
			return 1;
		case "*": case "/": case "%":
			return 2;
		case "^":
			return 3;
		}
		return -1;
	}
	
	/**
	 * Converts the infix expression to an postfix expression.
	 * @return the converted postfix expression.
	 */
	public String getPostfixExpression () {
		//Splits the expression up at every space.
		String expression[] = infix.split("\\s+");
		
		//Create the result stack.
		String result = "";
		
		//Create the stack for operators.
		StackInterface<String> stackOp = new ArrayStack<String>();

		//Iterates through the expression.
		for (int i = 0; i < expression.length; i++) {
			switch (expression[i]) {
			
			//Push ^ to the stack.
			case "^":
				stackOp.push(expression[i]);
				break;
				
			//Check the operators precedence, if it's greater pop and add the result, then push to the stack.
			case "+" : case "-": case "*": case "/": case "%": 
				while (!stackOp.isEmpty() && getOperatorRank(expression[i]) <= getOperatorRank(stackOp.peek())) {
					result += " " + stackOp.pop();
				}
				stackOp.push(expression[i]);
				break;
				
			//If it is the opening parenthesis, push to the stack.
			case "(":
				stackOp.push(expression[i]);
				break;
				
			//if it is closing parenthesis, pop everything up until a opening parenthesis is found.
			case ")" :
				String topOp = stackOp.pop();
				while (!topOp.equals("(")) {
					result += " " + topOp;
					topOp = stackOp.pop();
				}
				break;
				
			//If an operand is found, add the result.
			default:
				result += " " + expression[i];
				break;
			}
		}
		
		//If anything is still in the stack, pop it.
		while(!stackOp.isEmpty()) {
			result += " " + stackOp.pop();
		}
		
		//Trim and return the result.
		return result.trim();

	}
	
	/**
	 * Evaluates the infix expression and returns the result.
	 * @return returns the result of evaluation of the infix expression.
	 */
	public int evaluate () {
		//The stack that stores the operands
		StackInterface<Integer> operandStack = new ArrayStack<Integer>();
		
		//Splits the postfix expression at each space.
		String postfix[] = getPostfixExpression().split("\\s+");
		
		//Iterates through the entire postfix expression.
		for (int i = 0; i < postfix.length; i++) {
			switch (postfix[i]) {
			
				//If the character is a operator.
				case "+": case "-": case "*": case "/": case "^": case "%":
					
					/*Pops two operands and performs the calculation between the two
					operands and the operator.*/
					int op2 = Integer.valueOf(operandStack.pop());
					int op1 = Integer.valueOf(operandStack.pop());
					operandStack.push(evalHelp(op1, op2, postfix[i]));
					break;
					
				//If the character is not an operator (an operand).
				default:
					//Pushes the operand onto the stack.
					operandStack.push(Integer.parseInt(String.valueOf(postfix[i])));
					break;
			}
		}
		
		return operandStack.peek();
	}
	
	/**
	 * Helps the Evaluate method by performing an operation between two operands
	 * and a binary operator.
	 * @param one the first int.
	 * @param two the second int.
	 * @param operato the binary operator.
	 * @return
	 */
	private int evalHelp (int one, int two, String operator) {
		switch (operator) {
		case "+":
			return one + two;
		case "-":
			return one - two;
		case "*":
			return one * two;
		case "/":
			return one / two;
		case "^":
			return (int) Math.pow(one, two);
		case "%":
			return one % two;
		default: 
			return 0;
		}
	}
}
