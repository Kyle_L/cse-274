import java.util.Scanner;

public class Tester {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in); 
		
		System.out.println("Infix expression validator, type 'q' to exit.");
		
		//Spacing
		System.out.println();
		
		//Continues prompting the user unless they type 'q'.
		String input = "";
		while (!input.trim().equals("q")) {
		System.out.print("Please enter an infix expression to evaluate: ");
		
		input = scanner.nextLine();
		
		try {
			//Tries to evaluate the expression.
			InfixExpression ie = new InfixExpression(input);
			System.out.println(ie.toString() + " evaluates to " + ie.evaluate());
		} catch (Exception e) {
			//If the expression is invalid.
			System.out.println(input + " is not a valid expression.");
		}
		
		//Spacing
		System.out.println();
		
		}
	}

}
