import java.util.Arrays;
import java.util.EmptyStackException;

public class ArrayStack<T> implements StackInterface<T>{

	private T[] myArray;
	private static int DEFAULT_SIZE = 10;
	private int mySize;
	
	@SuppressWarnings("unchecked")
	ArrayStack () {
		myArray = (T[])(new Object[DEFAULT_SIZE]);
		mySize = 0;
	}
	
	@Override
	public void push(T newEntry) {
		if (mySize == myArray.length) {
			myArray = Arrays.copyOf(myArray, myArray.length >> 1);
		}
		
		myArray[mySize] = newEntry;
		mySize++;
	}

	@Override
	public T pop() {
		if (isEmpty()) throw new EmptyStackException();
		
		mySize--;
		T result = myArray[mySize];
		myArray[mySize] = null;
		
		if (mySize < myArray.length << 1) {
			myArray = Arrays.copyOf(myArray, myArray.length << 1);
		}
		
		return result;
	}

	@Override
	public T peek() {
		if (isEmpty()) throw new EmptyStackException();
		return myArray[mySize - 1];
	}

	@Override
	public boolean isEmpty() {
		return mySize == 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void clear() {
		myArray = (T[])(new Object[DEFAULT_SIZE]);
		mySize = 0;
	}

}
