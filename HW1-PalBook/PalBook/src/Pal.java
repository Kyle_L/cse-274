///////////////////////////////////////////////////////////////////////////////
//
// Title:            PalBook
// This File:        Pal.java
// Files:            PalBook.java, Pal.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (Pal.java) have been thoroughly tested and
//                   should perform the operations listed in PalBook.html. Please note, PalBook.java is dependent on this file.
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.ArrayList;
import java.util.Collections;

/**
 * The implementation of a Pal used in PalBook.
 * @author Kyle Lierer
 */
public class Pal implements Comparable<Pal>{

	private String myID;
	private ArrayList<Pal> myPals;

    /**
     * Creates a instance of Pal.
     * @param id The id of the pal.
     */
	public Pal (String id) {
		myID = id;
		myPals = new ArrayList<Pal>();
	}

    /**
     * Returns the id of the pal.
     * @return The id of the pal.
     */
	public String getID () {
		return myID;
	}

    /**
     * Adds a pal.
     * @param aPal The pal being added.
     * @return whether the pal was added or not.
     */
	public boolean addPal (Pal aPal) {
		if (myPals.add(aPal)) {
			
			//Sorts all pals if a pal is added.
			Collections.sort(myPals);
			
			return true;
		}
		return false;
	}

    /**
     * Removes a pal.
     * @param aPal The pal being removed.
     * @return Whether the pal was removed or not.
     */
	public boolean removePal(Pal aPal) {
		return myPals.remove(aPal);
	}

    /**
     * Returns of the pals of this pal.
     * @return Alls the pals of this pal.
     */
	public Pal[] getPals () {
		return  myPals.toArray(new Pal[myPals.size()]);
	}

    /**
     * Checks if a pal is a pal of this pal.
     * @param aPal The pal being checked if they're a pal of this pal.
     * @return Whether they're a pal of this pal or not.
     */
	public boolean checkPal (Pal aPal) {
		return myPals.contains(aPal);
	}
	
	 /**
	 * Indicates whether some other object is "equal to" this one by checking the obj's id with this one's.
	 * @param obj the reference object with which to compare.
	 * @return true if this object's id is the same as the obj argument's id; false otherwise.
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Pal) {
			Pal palObj = (Pal) obj;
			return palObj.getID() == myID;
		}
		return false;
	}

	/**
	 * Returns a hash code value for the object based on the object's id. This method is supported for the benefit of hash tables such as those provided by HashMap.
	 * @return a hash code value for this object.
	 */
	@Override
	public int hashCode() {
		return myID.hashCode();
	}

	/**
	 * Returns a string representation of the object's id.
	 * @return a string representation of the object.
	 */
	@Override
	public String toString () {
		return myID;
	}

	/**
	 * Compares this object with the specified object for order.
	 * @param other the pal to be compared.
	 * @return a negative integer, zero, or a positive integer as this object's id is less than, equal to, or greater than the specified object's id.
	 */
	@Override
    public int compareTo(Pal other) {
        return this.getID().compareTo(other.getID());
    }
}
