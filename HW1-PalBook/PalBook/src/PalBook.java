///////////////////////////////////////////////////////////////////////////////
//
// Title:            PalBook
// This File:        Pal.java
// Files:            PalBook.java, Pal.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (PalBook.java) have been thoroughly tested and
//                   should perform the operations listed in PalBook.html. Please note, this code is dependent on Pal.java.
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The implementation of the social media PalBook.
 * @author Kyle Lierer
 */
public class PalBook {

	private ArrayList<Pal> myPals;
	private Integer myCount;
	private final int MAX_COUNT;

	/**
	 *Creates an empty PalBook social network, with no members, and a max size of 500.
	 */
	public PalBook () {
		this(500);
	}

	/**
	 * Creates an empty PalBook social network, with no members, and a specified maximum size.
	 * @param maxSize The maximum number of members for the network.
	 */
	public PalBook(int maxSize) {
		myPals = new ArrayList<Pal>(maxSize);
		MAX_COUNT = maxSize;
		myCount = 0;
	}

	/**
	 * Adds a member to the social network.
	 * Returns true if, at the end of the method, that member is in the network, and false otherwise.
	 * Note that if the member already exists in the network, the member will not be added a second time, and the method will return true to indicate that the member is in the network.
	 * The only reason this method should fail is if the social network has reached its maximum size.
	 * @param id the id of the member to add. No two members may have the same id.
	 * @return true if the member is in the network at the completion of this method, and false if not.
	 */
	public boolean addMember(String id) {
		if (myCount >= MAX_COUNT) return false;

		//Adds a member to the network if they aren't already in it.
		if (!containsMember(id)) {
			myPals.add(new Pal(id));

			//Sorts the list.
			Collections.sort(myPals);

			//Increments count.
			myCount++;
		}

		return true;
	}

	/**
	 * Returns true if the two specified people exist in the network, and are pals, and returns false otherwise.
	 * @param id1 the id of a person.
	 * @param id2 the id of another person.
	 * @throws IllegalArgumentException if id1 or id2 aren't in the network.
	 * @return true if id1 and id2 are pals in the network, and false otherwise.
	 */
	public boolean arePals (String id1, String id2) {
	    //Checks if both people exist in the network.
		if (!containsMember(id1)|| !containsMember(id2)) return false;

		Pal pal1 = getPal(id1);
		Pal pal2 = getPal(id2);

		//Returns whether the two are both pals of each other.
		return pal1.checkPal(pal2) && pal2.checkPal(pal1);
	}

	/**
	 * Given two members, returns an array of members who are pals with both of the two members.
     * The array will be sorted by the members' ids.
	 * @param id1 the id of one member.
	 * @param id2 the id of another (not necessarily different) member.
	 * @return an array of members who are pals to both of the two specified members, sorted by id.
	 */
	public String[] commonPals (String id1, String id2) {
        //Checks if both people exist in the network.
        if (!containsMember(id1)|| !containsMember(id2)) return new String[0];

		Pal pal1 = getPal(id1);
		Pal pal2 = getPal(id2);

		//Gets the pals of both people.
		List<Pal> pals1 = Arrays.asList(pal1.getPals());
		List<Pal> pals2 = Arrays.asList(pal2.getPals());

		/*Iterates over pals1 and finds the ones that exist in pals2,
		those are then added to a commonPals.*/
		ArrayList<String> commonPals = new ArrayList<String>();
		for (Pal pal: pals1) {
			if (pals2.contains(pal))
				commonPals.add(pal.getID());
		}

		//Converts the list to an array and then returns that.
		return commonPals.toArray(new String[0]);
	}

	/**
	 * Returns true if there exists a member in the network with the given id, and false otherwise.
	 * @param id the id of a member.
	 * @return true if the member exists in the network, and false otherwise.
	 */
	public boolean containsMember (String id) {
		return indexOf(id) > -1;
	}

	/**
	 * Removes id1 from id2's pal list, and removes id2 from id1's pal list.
	 * Does nothing id1 or id2 are not members, or if they are members but are not pals.
	 * @param id1 the id of a person.
	 * @param id2 the id of another person.
	 * @throws IllegalArgumentException if id1 or id2 aren't in the social network.
	 */
	public void endPals (String id1, String id2) {
		if (!containsMember(id1)|| !containsMember(id2)) return;

		//Finds the pals associated with the ids.
		Pal pal1 = getPal(id1);
		Pal pal2 = getPal(id2);

		//Ends the pals relationship from both sides.
		pal1.removePal(pal2);
		pal2.removePal(pal1);
	}

	/**
	 * Gets a count of the number of members.
	 * @return the number of members in the social network.
	 */
	public int getMemberCount () {
		return myCount;
	}

	/**
	 * Returns a (possibly empty) array of all members in the network, sorted by id.
	 * @return an array of all members in the network, sorted by id. Returns an array of length 0 if there are no members in the network.
	 */
	public String[] getMembers () {
		//Reports all the pals into a string array.
		String result[] = new String[myCount];
		for (int i = 0; i < myCount; i++) {
			result[i] = myPals.get(i).toString();
		}
		return result;
	}

	/**
	 * Returns the pals of a particular member, sorted by id.
	 * @param id the id of a member.
	 * @return an array of the pals of the specified member, sorted by id.
	 * Returns an array of size zero if the specified member does not exist, or if the member exists but has no pals.
	 */
	public String[] getPals(String id) {
		if (!containsMember(id)) return new String[0];

		//Gets all the pals of a member.
		Pal pals[] = myPals.get(indexOf(id)).getPals();

		//Converts the pals to a string.
		String result[] = new String[pals.length];
	 	for (int i = 0; i < pals.length;i++) {
			result[i] = pals[i].getID();
	 	}
		return result;
	}

	/**
	 * If id1 and id2 are members in the network, then makes them pals.
	 * Returns true if, at the end of the method, id1 and id2 are pals (which could mean they were pals before this method was invoked), and returns false otherwise (which really would only happen if id1 or id2 were not a member in the network.
	 * @param id1 the id of a person.
	 * @param id2 the id of another person.
	 * @return if (indexOf(id1) < 0|| indexOf(id2) < 0) return false.
	 */
	public boolean makePals(String id1, String id2) {
		if (!containsMember(id1)|| !containsMember(id2) || id1 == id2) return false;
		Pal pal1 = myPals.get(indexOf(id1));
		Pal pal2 = myPals.get(indexOf(id2));

		pal1.addPal(pal2);
		pal2.addPal(pal1);
		return true;
	}

	/**
	 * Removes member from the social network. If member is pals with anyone, all traces of that member will be gone.
	 * That is, member will be removed from everyone's pal list. Does nothing if member is not in the social network.
	 * @param id the id of the member to be removed.
	 */
	public void removeMember(String id) {
		//If the pal doesn't exist, return.
		if (!containsMember(id)) return;

		//Gets the pal being removed.
		Pal pal = getPal(id);

		//Removes them from everyone's pal list.
		for (int i = 0; i < myPals.size(); i++) {
			myPals.get(i).removePal(pal);
		}

		//Removes them.
		myPals.remove(pal);

		//Decrements the count.
		myCount--;
	}

	/**
	 * Returns a newline-separated list of members, including a comma-separated list of each member's pals.
	 * Note that the list of members should be sorted by id, and each member's list of pals should be sorted by id.
	 * For example: Amy [Mary, Dina] Bob [Mary] Dina [Amy, Mary] Mary [Amy, Bob, Dina] Steve []
	 * @return a newline-separated list of members, with each member's pals. Members and pal lists sorted by id.
	 */
	@Override
	public String toString () {
		String result = "";

		for (int i = 0; i < myCount; i++) {
			Pal pal = myPals.get(i);
			result += pal.getID() + Arrays.toString(getPals(pal.getID())) + "\n";
		}

		return result;
	}

	/**
	 * Gets a pal from the social network.
	 * @param id the id of the member.
	 * @throws IllegalArgumentException if a member associated with id is not in the social network.
	 * @returns the member associated with the id.
	 */
	private Pal getPal (String id) {
		int index = indexOf(id);

		if (index < 0)
			throw new IllegalArgumentException();

		return myPals.get(index);
	}

	/**
	 * Finds the index of a member in the collection.
	 * @param id the id of the member.
	 * @return the index of the member.
	 */
	private int indexOf (String id) {
		for (int i = 0; i < myPals.size(); i++) {
			if (myPals.get(i).getID().equals(id)) return i;
		}
		return -1;
	}

}