import java.util.Arrays;

/**
 * A linked-node implementation of the Set ADT in which elements of the set
 * are always sorted (in this case, lexicographically, which is a fancy
 * way of saying "alphabetically").  Note that the String class has a compareTo
 * method which you should be using to assist you in keeping the set sorted.
 * @author Kyle Lierer
 */
public class SortedSet implements SetInterface<String> {

	private Node<String> myHead;
	private int myCount;
	
	/** Gets the current number of entries in this set.
    	@return  The integer number of entries currently in the set. */
	@Override
	public int getCurrentSize() {
		return myCount;
	}

	/** Sees whether this set is empty.
    	@return  True if the set is empty, or false if not. */
	@Override
	public boolean isEmpty() {
		return myCount == 0;
	}

	/** Adds a new entry to this set, avoiding duplicates.
		@param newEntry  The object to be added as a new entry.
    	@return  True if the addition is successful, or false if the item already is in the set. */
	@Override 
	public boolean add(String newEntry) {	
		//Creates the new node to be added.
		Node<String> newNode = new Node<String>(newEntry, null);
		
		//Case 1: If the head is null or the data comes before the head.
		if (myHead == null || myHead.getData().compareTo(newEntry) > 0) {
			newNode.setNext(myHead);
			myHead = newNode;
			myCount++;
			return true;
		}
		
		//Create a temporary node to iterate through the list.
		Node<String> temp = myHead;

		//Iterates through the list using currentNode.
		while (temp.getNext() != null) {
			//Case 2: The node is new and is getting placed in the middle of the list.
			if (temp.getNext().getData().compareTo(newEntry) > 0)
				break;
			//Case 3: If a node with equal value already exists, it doesn't get added.
			else if (temp.getNext().getData().compareTo(newEntry) == 0) 
				return false;
			//Case 4: The node is new and is getting placed at the end of the list.
			else
				temp = temp.getNext();
		}
		
		//Once in the correct spot, the data gets added by setting the currentNode's next to the new node.
		newNode.setNext(temp.getNext());
		temp.setNext(newNode);
		myCount++;
		
		return true;
	}

	/** Removes a specific entry from this set, if possible.
    	@param anEntry  The entry to be removed.
    	@return  True if the removal was successful, or false if not. */
	@Override
	public boolean remove(String anEntry) {	
		//Case 1: The list is empty.
		if (isEmpty()) return false;

		//Case 2: The head is equal to the anEntry.
		if(myHead.getData().compareTo(anEntry) == 0) {
			myHead = myHead.getNext();
			myCount--;
			return true;
		}
		
		Node<String> temp = myHead, previousTemp = myHead;

		//Case 3: An element in the middle of end is equal to anEntry.
		while (temp.getNext() != null) {
			previousTemp = temp;
			temp = temp.getNext();
			if (temp.getData().compareTo(anEntry) == 0) {
				previousTemp.setNext(temp.getNext());
				myCount--;
				return true;
			}
		}
		
		//Case 4: The element is not in the list.
		return false;		
	}

	/** Removes one unspecified entry from this set, if possible.
    	@return  Either the removed entry, if the removal was successful, or null. */
	@Override
	public String remove() {
		if (isEmpty()) return null;

		String result = myHead.getData();
		myHead = myHead.getNext();
		myCount--;
		return result;	
	}

	/** Removes all entries from this set. */
	@Override
	public void clear() {
		myHead = null;
	}

	/** Tests whether this set contains a given entry.
    	@param anEntry  The entry to locate.
    	@return  True if the set contains anEntry, or false if not. */
	@Override
	public boolean contains(String anEntry) {
		if(isEmpty()) return false;
		
		//Create a temporary node to iterate through the list.
		Node<String> temp = myHead;	
		
		//Iterates through the list to find the node with data equal to anEntry.
		while (temp != null) {
			if (temp.getData().equals(anEntry)) return true;
			temp = temp.getNext();
		}

		return false;
	}
	
	/*
	 * returns a string representation of the items in the bag,
	 * specifically a space separated list of the strings, enclosed
	 * in square brackets [].  For example, if the set contained
	 * cat, dog, then this should return "[cat dog]".  If the
	 * set were empty, then this should return "[]".
	 */
	@Override
	public String toString() {
		return Arrays.toString(toArray());
	}
	
	/** Retrieves all entries that are in this set.
	 	@return  A newly allocated array of all the entries in the set. */
	@Override
	public String[] toArray() {
		//Create an array to copy entries into.
		String result[] = new String[myCount];
		
		//Create a temporary node to iterate through the list.
		Node<String> temp = myHead;
		
		/*Iterates through the list while the current node is not
		 * equal to null and puts each entry into its expected spot
		 * in the array.*/
		for (int i = 0; temp != null; i++) {
			result[i] = temp.getData();
			temp = temp.getNext();
		}
		
		return result;
	}

}
