
public class SetTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		SortedSet set = new SortedSet();
		
		//Test adding.
		System.out.println("**Testing adding**");
		
		for (char c = 'a'; c <= 'g'; c+=2) {
			System.out.println("Adding " + c + " to the set. Expected true. Added? " + set.add(Character.toString(c)));
		}
		
		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
		
		//Test adding out of order.
		System.out.println("**Testing adding out of order**");
				
		for (char c = 'b'; c <= 'g'; c+=2) {
			System.out.println("Adding " + c + " to the set. Expected true. Added? " + set.add(Character.toString(c)));
		}
		
		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
		
		//Test adding duplicates.
		System.out.println("**Testing adding duplicates**");
				
		for (char c = 'b'; c <= 'g'; c+=2) {
			System.out.println("Adding " + c + " to the set. Expected false. Added? " + set.add(Character.toString(c)));
		}
		
		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
		
		//Testing contains.
		System.out.println("**Testing contains**");
		System.out.println("Contains a? Expected true: " + set.contains("a"));
		System.out.println("Contains d? Expected true: " + set.contains("d"));
		System.out.println("Contains z? Expected false: " + set.contains("z"));
		System.out.println();
		
		//Testing remove.
		System.out.println("**Testing remove**");
		System.out.println("Remove a. Expected true: " + set.remove("a"));
		System.out.println("Remove g. Expected true: " + set.remove("g"));
		System.out.println("Remove c. Expected true: " + set.remove("c"));
		System.out.println("Remove unspecified. removed " + set.remove());
		
		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
		
		//Testing adding after remove
		System.out.println("**Testing adding after removing**");
		
		System.out.println("Adding g to the set. Expected true. Added? " + set.add("g"));
		System.out.println("Adding c to the set. Expected true. Added? " + set.add("c"));

		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
		
		//Testing remove all.
		System.out.println("**Testing remove all**");
		while(!set.isEmpty()) {
			System.out.println("Removed " + set.remove() + ".");
		}
		
		System.out.println();
		System.out.println("Current state of the set: " + set.toString());
		System.out.println("Current size of the set: " + set.getCurrentSize());
		System.out.println();
	
		System.out.println("**Testing remove when empty**");
		System.out.println("Remove unspecified. Expected null: " + set.remove());
		System.out.println("Remove \"b\". Expected false: " + set.remove("b"));
		
	}

}
