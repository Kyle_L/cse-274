import java.util.ArrayList;
import java.util.Collections;

/**
 * Your implementation of the LookupInterface.  The only public methods
 * in this class should be the ones that implement the interface.  You
 * should write as many other private methods as needed.  Of course, you
 * should also have a public constructor.
 * 
 * @author Kyle Lierer
 */
public class StudentLookup implements LookupInterface {
	
	private BinaryTree<String> myTree;
	private ArrayList<BinaryTree<String>.Node<String>> myList;
	private boolean isListUpToDate = false;

	public StudentLookup() {
		//Creates the binary tree, the main way strings are stored.
		myTree = new BinaryTree<>();
	}

	@Override
	public void addString(int amount, String s) {
		//Adds a string to the tree.
		myTree.add(s, amount);	
		
		//Indicates the list is not up to date.
		isListUpToDate = false;
	}

	@Override
	public int lookupCount(String s) {
		//Finds the count in the tree.
		return myTree.getCount(s);
	}

	@Override
	public String lookupPopularity(int n) {
		/*Only updates the list if the tree has been changed,
		 * otherwise the list doesn't need updated.*/
		if (!isListUpToDate) {
			//Converts the tree to a list.
			myList = myTree.toList();
			
			//Sorts the list (note, sorts by occurrences of nodes).
			Collections.sort(myList);

			//Indicates the list is now up to date.
			isListUpToDate = true;
		}
		//Returns the popularity.
		return myList.get(n).myValue;
	}

	@Override
	public int numEntries() {
		//Returns the number of nodes in the tree.
		return myTree.getNumberOfNodes();
	}

}
