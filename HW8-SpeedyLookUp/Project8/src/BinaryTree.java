import java.util.ArrayList;

/**
 * A binary tree that is specifically designed for StudentLookUp.java.
 * @author Kyle Lierer
 *
 * @param <T>
 */
public class BinaryTree<T extends Comparable<? super T>> {

	@SuppressWarnings("hiding")
	class Node<T> implements Comparable<Node<T>> {
		int myCount;
	    T myValue;
	    Node<T> myLeft;
	    Node<T> myRight;
	 
	    Node(T value) {
	    	myCount = 1;
	        this.myValue = value;
	        myRight = null;
	        myLeft = null;
	    }

	    /**
	     * Compares a node to another node based on the number
	     * of occurrences. A node with greater occurrences is greater
	     * than a node with less occurrences.
	     */
		@Override
		public int compareTo(Node<T> aNode) {
			return aNode.myCount - myCount;
		}
	} 
	
	private Node<T> myRoot;
	private int myCount;
	
	/**
	 * Constructs an instance of the binary tree.
	 */
	public BinaryTree() {
		myRoot = null;
		myCount = 0;
	}
	
	/**
	 * Adds a value to the binary tree.
	 * @param value what is being added to the tree.
	 * @param amount what many of those values are being added to the tree.
	 */
	public void add (T value, int amount) {
		Node<T> node = new Node<T>(value);
		node.myCount += amount - 1;
		
		myRoot = add(myRoot, value, amount);
	}

	/**
	 * The private helper method to add.
	 * Adds a value to the binary tree.
	 * @param newNode the node that represents the current position in the tree.
	 * @param value what is being added to the tree.
	 * @param amount what many of those values are being added to the tree.
	 * @return a reference to the current node, typically the same unless newNode = null.
	 */
	private Node<T> add(Node<T> newNode, T value, int amount) {
	    if (newNode == null) {
	    	myCount += amount;
	    	Node<T> node = new Node<T>(value);
	    	node.myCount += amount - 1;
	        return node;
	    }
	 
	    if (value.compareTo((T) newNode.myValue) < 0) {
	        newNode.myLeft = add(newNode.myLeft, value, amount);
	    } else if (value.compareTo((T) newNode.myValue) > 0) {
	        newNode.myRight = add(newNode.myRight, value, amount);
	    }  else {
	    	newNode.myCount++;
	    }
	    
	    return newNode;
	}

	/**
	 * Returns the number of occurrences of a specific value.
	 * @param value the specific value.
	 * @return the number of occurrences.
	 */
	public int getCount(Comparable<T> value) {
	    return getCount(myRoot, value);
	}

	/**
	 * Returns the number of occurrences of a specific value.
	 * @param root represents the current position in the tree.
	 * @param value the specific value.
	 * @return the number of occurrences.
	 */
	private int getCount(Node<T> root, Comparable<T> value) {
	    if (root == null) {
	        return 0;
	    } 
	    if (value.equals(root.myValue)) {
	        return root.myCount;
	    } 
	    return (value.compareTo((T) root.myValue) < 0)
	      ? getCount(root.myLeft, value)
	      : getCount(root.myRight, value);
	}

	/**
	 * Returns then number of nodes in the tree.
	 * @return the number of nodes in the tree.
	 */
	public int getNumberOfNodes() {
		return getNumberOfNodes(myRoot);
	}

	/**
	 * The private helper method of getNumberOfNodes.
	 * Returns the number of occurrences of a specific value.
	 * @param root represents the current position in the tree.
	 * @return the number of nodes in the tree.
	 */
	private int getNumberOfNodes (Node<T> root) {
		if (root == null) return 0;
		
		return 1 + getNumberOfNodes(root.myLeft) + getNumberOfNodes(root.myRight);
	}

	/**
	 * Converts the tree to an ArrayList of Node<T>.
	 * @return returns the ArrayList.
	 */
	public ArrayList<Node<T>> toList () {
		ArrayList<Node<T>> list = new ArrayList<Node<T>>();
		toList(list, myRoot);
		return list;
	}

	/**
	 * The private helper method of toList.
	 * Converts the tree to an ArrayList of Node<T>.
	 * @param list the list that is being added to.
	 * @param root the current position in the tree.
	 */
	private void toList (ArrayList<Node<T>> list, Node<T> root) {
		if (root == null) return;
		toList(list, root.myLeft);		
		list.add(root);
		toList(list, root.myRight);
	}

}

