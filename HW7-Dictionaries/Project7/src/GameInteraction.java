///////////////////////////////////////////////////////////////////////////////
//
// Title:            Tic Tac Toe
// This File:        Game.java
// Files:            Game.java, GameInteraction.java, Board.java, DictionaryInterface.java, HashedDictionary.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (GameInteraction.java) have been thoroughly tested and
//                   should perform the implemented operations.
//
// Starting Point:	 The main method of the program is located in GameInteraction.java.
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Scanner;

/***
 * Represent the user's interactions with the game, Tic Tac Toe.
 * @author Kyle Lierer
 *
 */
public class GameInteraction {
		
	public static void main(String[] args) {
		
		//Initiates the scanner.
		Scanner sc = new Scanner(System.in);
		
		//Continues playing the game until broken out of.
		while (true) {
			//Prompts user if they'd like to play.
			char playGame = ' ';
			while (playGame != 'Y' && playGame != 'N') {
				System.out.print("Would you like to play a game? (Y, N): ");
				playGame = sc.nextLine().trim().toUpperCase().charAt(0);
			}
			
			//If 'N', stop.
			if (playGame == 'N') return;
			
			//Create the game and go.
			Go();
		}
	}
	
	/**
	 * Plays a single game of Tic Tac Toe.
	 */
	public static void Go () {		
		//Initiates the scanner.
		Scanner sc = new Scanner(System.in);
		
		//Declares the game.
		Game game;
		
		//Prompts the user with what player they want to be.
		char player = ' ';
		while (player != 'X' && player != 'O') {
			System.out.print("Would you either like to play 'X' or 'O'? (X, O): ");
			player = sc.nextLine().trim().toUpperCase().charAt(0);
		}

		System.out.println();		
		
		//If the user chose 'X'.
		if (player == 'X') {
			game = new Game(true);
			
			//Prints the initial board.
			System.out.println("Here's the initial game board.");
			System.out.println(game.toString());

			//Plays the game.
			while (true) {
				
				System.out.println("Place your token.");
				//Asks the user for a row.
				int row;
				//Asks the user for a col.
				int col;
				
				//Keeps track of if the spot is occupied.
				boolean tokenPlaced;
				
				do {
					row = -1;
					while (row < 0 || row > 2) {
						System.out.print("Row (0 - 2): ");
						try {
							row = Integer.parseInt(sc.nextLine());
						} catch (NumberFormatException e) {
							continue;
						}
					}
					col = -1;
					while (col < 0 || col > 2) {
						System.out.print("Column (0 - 2): ");
						try {
							col = Integer.parseInt(sc.nextLine());
						} catch (NumberFormatException e) {
							continue;
						}
					} 
					
					tokenPlaced = game.placeToken(row, col);
					
					if (!tokenPlaced) System.out.println("That spot is occupied, please choose another spot.");
					
				} while (!tokenPlaced);
				
				System.out.println();

				//Place token and display player's move.
				System.out.println("The board after your move.");
				System.out.println(game.toString());				
				System.out.println();
				
				//Check for win.
				if (game.isGameOver()) break;
				
				//Place enemy token and play enemy's move.
				System.out.println("The board after O's move.");
				game.placeEnemyToken();
				System.out.println(game.toString());
				System.out.println();
				
				//Check for win.
				if (game.isGameOver()) break;
			}			
			
		//If the user chose 'O'.
		} else {
			game = new Game(false);
			
			//Displays the initial board
			System.out.println("Here's the initial game board.");
			System.out.println(game.toString());
				
			//Plays the game.
			while (true) {
				//Displays the board after the enemy's move.				
				System.out.println("The board after O's move.");
				game.placeEnemyToken();
				System.out.println(game.toString());
				System.out.println();
				
				//Check for win.
				if (game.isGameOver()) break;
				
				System.out.println("Place your token.");
				//Asks the user for a row.
				int row;
				//Asks the user for a col.
				int col;
				
				//Keeps track of if the spot is occupied.
				boolean tokenPlaced;
				
				do {
					row = -1;
					while (row < 0 || row > 2) {
						System.out.print("Row (0 - 2): ");
						try {
							row = Integer.parseInt(sc.nextLine());
						} catch (NumberFormatException e) {
							continue;
						}
					}
					col = -1;
					while (col < 0 || col > 2) {
						System.out.print("Column (0 - 2): ");
						try {
							col = Integer.parseInt(sc.nextLine());
						} catch (NumberFormatException e) {
							continue;
						}
					} 
					
					tokenPlaced = game.placeToken(row, col);
					
					if (!tokenPlaced) System.out.println("That spot is occupied, please choose another spot.");
					
				} while (!tokenPlaced);
				
				System.out.println();

				//Displays the board after player's move.
				System.out.println("The board after your move.");
				game.placeToken(row, col);
				System.out.println(game.toString());				
				System.out.println();
				
				//Check for a win.
				if (game.isGameOver()) break;
			}

		}
		
		//Displays the winning character.
		char c = game.winningPlayer();
		System.out.println("Who won the game? " + ((c == '-') ? "Draw" : c));
	}
	
}
