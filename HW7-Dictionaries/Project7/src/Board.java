///////////////////////////////////////////////////////////////////////////////
//
// Title:            Tic Tac Toe
// This File:        Game.java
// Files:            Game.java, GameInteraction.java, Board.java, DictionaryInterface.java, HashedDictionary.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (Board.java) have been thoroughly tested and
//                   should perform the implemented operations.
//
// Starting Point:	 The main method of the program is located in GameInteraction.java.
//
//////////////////////////// 80 columns wide //////////////////////////////////

import java.util.Arrays;

/**
 * Represents an n by n TicTacToe board.
 * @author Kyle Lierer
 *
 */
public class Board {

	private char[] myBoard;
	private final int myRows;
	private final int myCols;
	
	private static final char EMPTY_CHAR = '-';
	private static final int DEFAULT_ROWS = 3;	
	private static final int DEFAULT_COLS = 3;
	
	/**
	 * Creates a board using a String.
	 * @param board The string representation of the board. such as "--XX-OO--"
	 */
	public Board() {
		this(DEFAULT_ROWS, DEFAULT_COLS);
	}
	
	public Board(int rows, int cols) {
		if (rows <= 0 || cols <= 0) throw new IllegalArgumentException();
		myRows = rows;
		myCols = cols;
		myBoard = new char[myRows * myCols];
		
		Arrays.fill(myBoard, EMPTY_CHAR);
	}
	
	public Board (Board aBoard) {
		this.myBoard = Arrays.copyOf(aBoard.myBoard, aBoard.myBoard.length);
		this.myRows = aBoard.myRows;
		this.myCols = aBoard.myCols;
	}
	
	public int getRows() {
		return myRows;
	}

	public int getCols() {
		return myCols;
	}
	
	public int getSize () {
		return myBoard.length;
	}
	
	public boolean isFull () {
		for (int i = 0; i < myBoard.length; i++) {
			if (myBoard[i] == EMPTY_CHAR) return false;
		}
		return true;
	}

	public void buildFromString(String aString) {
		if (aString.length() != myRows * myCols) throw new IllegalArgumentException();
		
		char[] chars = aString.toCharArray();
		myBoard = chars;
	}
	
	public void placeToken(int index, char token) {
		if (index < 0 || index >= myBoard.length) throw new IllegalArgumentException();
		myBoard[index] = token;
	}
	
	public void placeToken(int row, int col, char token) {
		if (row > myRows - 1 || col > myCols - 1 || row < 0 || col < 0) throw new IllegalArgumentException();
		placeToken(row * myRows + col, token);
	}

	public char getToken (int index) {
		if (index < 0 || index >= myBoard.length) throw new IllegalArgumentException();
		return myBoard[index];
	}
	
	public char getToken (int row, int col) {
		if (row > myRows - 1 || col > myCols - 1 || row < 0 || col < 0) throw new IllegalArgumentException();
		return getToken(row * myRows + col);
	}
	
	public boolean isLocationEmpty (int row, int col) {
		return getToken(row, col) == EMPTY_CHAR;
	}
	
	public boolean isLocationEmpty (int index) {
		return getToken(index) == EMPTY_CHAR;
	}
	
	public int[] getPossibleMoves () {
		int[] results = new int[myBoard.length];
		int mySize = 0;
		
		for (int i = 0; i < myBoard.length; i++) {
			if (myBoard[i] == EMPTY_CHAR) {
				results[mySize] = i;
				mySize++;
			}
		}
		return Arrays.copyOf(results, mySize);
	}
	
	@Override
	public String toString() {
		String result = "";
		for (int i = 0; i < myBoard.length; i++) {
			result += myBoard[i];
		}
		return result;
	}
	
	/**
	 * Returns the board as string with each row as it's own line.
	 * @return
	 */
	public String toBoardString () {
		String result = "";
		for (int i = 1; i <= myBoard.length; i++) {
			result += myBoard[i - 1];
			if (i % myCols == 0) result += "\n";
		}
		return result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(myBoard);
		result = prime * result + myCols;
		result = prime * result + myRows;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Board)) return false;
		
		Board other = (Board) obj;
		if (!Arrays.equals(myBoard, other.myBoard)) return false;
		if (myCols != other.myCols) return false;
		if (myRows != other.myRows) return false;
		return true;
	}
}
