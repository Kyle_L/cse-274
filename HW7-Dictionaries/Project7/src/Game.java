///////////////////////////////////////////////////////////////////////////////
//
// Title:            Tic Tac Toe
// This File:        Game.java
// Files:            Game.java, GameInteraction.java, Board.java, DictionaryInterface.java, HashedDictionary.java
// Semester:         Fall 2018
//
// Author:           Kyle Lierer
// Email:            liererkt@gmail.com
// Class:            CSE 274 E
// Instructor:       Dr. Manar Mohamed
//
//
// Working Parts:    All parts of this code (Game.java) have been thoroughly tested and
//                   should perform the implemented operations.
//
// Starting Point:	 The main method of the program is located in GameInteraction.java.
//
//////////////////////////// 80 columns wide //////////////////////////////////

/**
 * Represents a game of Tic Tac Toe.
 * @author Kyle Lierer
 *
 */
public class Game {

	private char myPlayer = 'X';
	private Board myBoard;
	private DictionaryInterface<Board, Integer> allBoard;
	
	private static final int TOKENS_NEEDED_TO_WIN = 3;	
	
	/**
	 * Creates an instance of the Tic Tac Toe game.
	 * @param isPlayingX whether the user's player is X or O.
	 */
	public Game(boolean isPlayingX) {
		myPlayer = (isPlayingX) ? 'X' : 'O';
		myBoard = new Board();
		allBoard = new HashedDictionary<Board, Integer>();		
		createAllBoards();

	}
	
	/**
	 * Places the player's token on the board.
	 * @param row The row where the token is placed.
	 * @param col The col where the token is paced.
	 * @return Whether the token was successfully placed.
	 */
	public boolean placeToken (int row, int col) {
		//Don't place if the board is full or the row, col is taken.		
		if (myBoard.isFull()) return false;
		if (myBoard.getToken(row, col) != '-') return false;
		
		myBoard.placeToken(row, col, myPlayer);
						
		return true;
	}	

	/**
	 * Places an enemy token on the board at the best place for the enemy.
	 * @return Whether the enemy token was placed successfully.
	 */
	public boolean placeEnemyToken () {
		int index = getBestMove(myBoard);
		
		//Don't place if the board is full or the bestMove spot is full.
		if (myBoard.isFull()) return false;
		if (myBoard.getToken(index) != '-') return false;
		
		myBoard.placeToken(index, getEnemyCharacter());
		
		return true;
	}
	
	/**
	 * A recursive method that creates all possible valid boards. 
	 */
	private void createAllBoards() {
		createAllBoards(new Board(), myPlayer);
	}
	
	private void createAllBoards (Board board, char player) {
		//If the board already exists, stop.
		if (allBoard.contains(board)) return;
		
		//Iterate through all possible moves.
		for (int move : board.getPossibleMoves()) {
			Board temp = new Board(board);
			temp.placeToken(move, player);
			
			//If the game is won, stop.
			if (isGameWon(temp, player)) {
				allBoard.add(temp, -1);
			} else if (!temp.isFull()) {
				//If the game is not won, continue building boards.
				createAllBoards(temp, (player == 'X') ? 'O' : 'X');	
				allBoard.add(board, getBestMove(board, player));
			}
		}
	}
	
	/**
	 * Gets the best move for the player.
	 * @return The index of the best move.
	 */
	public int getBestMove () {
		return getBestMove(myBoard);
	}
	
	/***
	 * Gets the best move based on a particular board.
	 * @param board The board for which the best move is being gotten from.
	 * @return The index of the best.
	 */
	public int getBestMove (String board) {
		Board b = new Board();
		b.buildFromString(board);
		return getBestMove(b);
	}
	
	private int getBestMove (Board board) {
		return allBoard.getValue(myBoard);
	}

	private int getBestMove (Board board, char player) {
		int move = -1;
		
		//Check if the game is about to be won by the player.
		move = isGameAboutToBeWon(board, player);
		
		//Check if the game is about to be won by the enemy.
		if (move < 0) move = isGameAboutToBeWon(board, (player == 'O') ? 'X' : 'O');
		
		//Get a generic move if the game is not about to be won.
		if (move < 0) move = getBestGenericMove(board);
		return move;
	}
	
	/**
	 * Returns the winning player ('X', 'O', or '-' for a tie).
	 * @return The winning player as a char.
	 */
	public char winningPlayer () {
		char c = '-';
		//If the game is not won, return '-'.
		if (!isGameWon()) return c;
		
		//If the game is won by 'X'.
		if (isGameWon(myBoard, 'X')) return 'X';
		
		//If the game is won by 'O'.
		if (isGameWon(myBoard, 'O')) return 'O';
		return '-';
	}
	
	/**
	 * Returns whether someone won the game.
	 * @return Whether someone won the game.
	 */
	public boolean isGameWon () {
		//Returns true if 'X' or 'O' won the game.
		return isGameWon(myBoard, myPlayer) || isGameWon(myBoard, (myPlayer == 'X') ? 'O' : 'X');
	}

	/**
	 * Returns whether a player has won a specific board.
	 * @param board The board being checked if it's won.
	 * @param player The player that is being if they won the board.
	 * @return Whether player won the specific board.
	 */
	private boolean isGameWon(Board board, char player) {
		int count = 0;
		
		//Rows.
		for (int i = 0; i < board.getRows(); i++) {
			count = 0;
			for (int j = 0; j < TOKENS_NEEDED_TO_WIN; j++) {
				if(board.getToken(i, j) == player) count++;
			}
			if (count == TOKENS_NEEDED_TO_WIN) return true;
		}
		
		//Cols.
		for (int i = 0; i < board.getCols(); i++) {
			count = 0;
			for (int j = 0; j < TOKENS_NEEDED_TO_WIN; j++) {
				if(board.getToken(j , i) == player) count++;
				if (count == TOKENS_NEEDED_TO_WIN) return true;
			}
		}
		
		//Diag.
		count = 0;
		for (int i = 0; i < TOKENS_NEEDED_TO_WIN; i++) {
			if (board.getToken(i, i) == player) count++;
			if (count == TOKENS_NEEDED_TO_WIN) return true;
		}
		
		//Anti-diag.
		count = 0;
		for (int i = 0; i < TOKENS_NEEDED_TO_WIN; i++) {
			if (board.getToken(i, TOKENS_NEEDED_TO_WIN - i - 1) == player) count++;
			if (count == TOKENS_NEEDED_TO_WIN) return true;
		}
		
		return false;

	}
	
	private int isGameAboutToBeWon (Board board, char type) {
		for (int move : board.getPossibleMoves()) {
			Board temp = new Board(board);
			temp.placeToken(move, type);
			if (isGameWon(temp, type)) {
				return move;
			}
		}
		return -1;
	}
	
	/***
	 * Whether a win is possible by either player.
	 * @return Whether a win is possible by either player.
	 */
	public boolean isWinPossible () {
		return isWinPossible(myBoard, myPlayer);
	}
	
	/**
	 * Whether a win is possible by either player.
	 * @param board The initial board.
	 * @param player The initial player.
	 * @return
	 */
	private boolean isWinPossible (Board board, char player) {
		boolean didFindWin = false;
		for (int move : board.getPossibleMoves()) {
			Board temp = new Board(board);
			temp.placeToken(move, player);
			if (isGameWon(temp, player)) {
				didFindWin = true;
			} else if (!temp.isFull()) {
				didFindWin = didFindWin || isWinPossible(temp, (player == 'X') ? 'O' : 'X');	
			}
		}
		return didFindWin;
	}

	/**
	 * Returns whether the game is over or not.
	 * @return Whether the game is over or not.
	 */
	public boolean isGameOver () {
		return !isWinPossible() || isGameWon();
	}
	
	private int getBestGenericMove (Board board) {
		//1 Center
		if (board.isLocationEmpty(board.getSize()/2)) { return board.getSize()/2; }		
		
		//2 Corners
		if (board.isLocationEmpty(0, 0)) { return 0; }	
		if (board.isLocationEmpty(0, board.getCols() - 1)) { return board.getCols() - 1; }	
		if (board.isLocationEmpty(board.getRows() - 1, 0)) { return board.getSize() - board.getCols() - 1; }	
		if (board.isLocationEmpty(board.getRows() - 1, board.getCols() - 1)) { return board.getSize() - 1; }	

		//3 Edges
		for (int i = 1; i < board.getCols(); i++) {
			if (board.isLocationEmpty(0, i)) { return i; }
		}
		for (int i = 1; i < board.getRows(); i++) {
			if (board.isLocationEmpty(i, 0)) { return i; }
		}
		for (int i = 1; i < board.getCols(); i++) {
			if (board.isLocationEmpty(board.getCols() - 1, i)) { return i; }
		}
		for (int i = 1; i < board.getRows(); i++) {
			if (board.isLocationEmpty(i, board.getRows() - 1)) { return i; }
		}
		
		return -1;
	}
	
	/**
	 * Returns the enemy's char.
	 * @return The enemy's char.
	 */
	private char getEnemyCharacter () {
		return (myPlayer == 'X') ? 'O' : 'X';
	}
	
	@Override
	public String toString() {
		return myBoard.toBoardString();
	}
}
