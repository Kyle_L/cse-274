/**
 * A class that implements the ADT list by using a chain of linked nodes that
 * has a head reference.
 * 
 * @author Frank M. Carrano
 * @author Timothy M. Henry
 * @author MODIFIED BY NORM KRUMPE AND LATER MODIFIED BY KYLE LIERER
 * @version 4.0
 */
public class LListTail<T> implements ListInterface<T> {
	
	private Node lastNode;
	private int numberOfEntries;

	/**
	 * Creates an instance LListTail, a circular inked list.
	 */
	public LListTail() {
		initializeDataFields();
	}

	/**
	 * {@inheritDoc}}
	 */
	public void clear() {
		initializeDataFields();
	}

	/**
	 * {@inheritDoc}}
	 * **OutOfMemoryError possible.**
	 */
	public void add(T newEntry) {
		Node newNode = new Node(newEntry);

		// Add to empty list.
		if (isEmpty()) {
			lastNode = newNode;
			lastNode.setNextNode(lastNode);
			
		// Add to end of non-empty list.
		} else {
			newNode.setNextNode(lastNode.next);
			lastNode.setNextNode(newNode);
			lastNode = newNode;
		}

		numberOfEntries++;
	}

	/***
	 * {@inheritDoc}
	 */
	public void add(int newPosition, T newEntry) {		
		if ((newPosition >= 1) && (newPosition <= numberOfEntries + 1)) {
			Node newNode = new Node(newEntry);

			//Case 1: List empty.
			if (isEmpty()) {
				lastNode = newNode;
				lastNode.setNextNode(lastNode);
			// Case 2: adding at end.
			} else if (newPosition == numberOfEntries + 1) {	
				newNode.setNextNode(lastNode.next);
				lastNode.setNextNode(newNode);
				lastNode = newNode;
			
			// Case 2: list is not empty and newPosition > 1
			} else { 
				Node nodeBefore = getNodeAt((newPosition - 1 <= 0) ? numberOfEntries : newPosition - 1);
				Node nodeAfter = nodeBefore.getNextNode();
				newNode.setNextNode(nodeAfter);
				nodeBefore.setNextNode(newNode);
			}

			numberOfEntries++;
		} else {
			throw new IndexOutOfBoundsException("Illegal position given to add operation.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T remove(int givenPosition) {
		T result = null; // Return value

		if ((givenPosition >= 1) && (givenPosition <= numberOfEntries)) {

			//Case 1: Only 1 element
			if (numberOfEntries == 1) {
				result = lastNode.getData();
				lastNode = null;
			// Case 2: Remove last entry
			} else if (givenPosition == numberOfEntries + 1) {
				result = lastNode.getData(); // Save entry to be removed
				lastNode = lastNode.next; // Remove entry
			
			// Case 2: Not first entry
			} else {
				Node nodeBefore = getNodeAt((givenPosition - 1 <= 0) ? numberOfEntries : givenPosition - 1);
				Node nodeToRemove = nodeBefore.getNextNode();
				result = nodeToRemove.getData(); // Save entry to be removed
				Node nodeAfter = nodeToRemove.getNextNode();
				nodeBefore.setNextNode(nodeAfter); // Remove entry
			}

			numberOfEntries--; // Update count
			return result; // Return removed entry
		} else {
			throw new IndexOutOfBoundsException("Illegal position given to remove operation.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T replace(int givenPosition, T newEntry) {
		//Checks if the givenPosition is within the bounds of the list.
		if ((givenPosition >= 1) && (givenPosition <= numberOfEntries)) {
			Node desiredNode = getNodeAt(givenPosition);
			T originalEntry = desiredNode.getData();
			desiredNode.setData(newEntry);
			return originalEntry;
			
		//If givenPosition is outside the bounds, throw an error.
		} else {
			throw new IndexOutOfBoundsException("Illegal position given to replace operation.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T getEntry(int givenPosition) {
		//Checks if the givenPosition is within the bounds of the list.
		if ((givenPosition >= 1) && (givenPosition <= numberOfEntries)) {
			return getNodeAt(givenPosition).getData();
			//If givenPosition is outside the bounds, throw an error.
		} else {
			throw new IndexOutOfBoundsException("Illegal position given to getEntry operation.");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public T[] toArray() {
		// The cast is safe because the new array contains null entries
		@SuppressWarnings("unchecked")
		T[] result = (T[]) new Object[numberOfEntries];

		int index = 0;
		Node currentNode = lastNode.next;
		while ((index < numberOfEntries) && (currentNode != null)) {
			result[index] = currentNode.getData();
			currentNode = currentNode.getNextNode();
			index++;
		}

		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean contains(T anEntry) {
		int count = 0;
		boolean found = false;
		Node currentNode = lastNode;

		/*Iterates through the list and checks if the
		 data in the nodes is equal to anEntry.*/
		while (!found && (currentNode != null)) {
			if (anEntry.equals(currentNode.getData())) {
				found = true;
			} else {
				currentNode = currentNode.getNextNode();
				count++;
				if (count > numberOfEntries) return false;
			}
		}

		return found;
	}

	/**
	 * {@inheritDoc}
	 */
	public int getLength() {
		return numberOfEntries;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isEmpty() {
		boolean result;

		// Or getLength() == 0
		if (numberOfEntries == 0) {
			result = true;
		} else {
			result = false;
		} // end if

		return result;
	}

	/**
	 * Initializes the class's data fields to indicate an empty list.
	 */
	private void initializeDataFields() {
		lastNode = null;
		numberOfEntries = 0;
	}

	/**
	 * Returns a reference to the node at a given position.
	 * @param givenPosition
	 * @return a reference to the node at a given position.
	 * Precondition: The chain is not empty;
	 * 1 <= givenPosition <= numberOfEntries.
	 */
	private Node getNodeAt(int givenPosition) {
		Node currentNode = lastNode.next;

		// Traverse the chain to locate the desired node
		// (skipped if givenPosition is 1)
		for (int counter = 1; counter < givenPosition; counter++) {
			currentNode = currentNode.getNextNode();
		}
		
		return currentNode;
	}

	private class Node {
		private T data; // Entry in list
		private Node next; // Link to next node

		private Node(T dataPortion) {
			data = dataPortion;
			next = null;
		}

		private Node(T dataPortion, Node nextNode) {
			data = dataPortion;
			next = nextNode;
		}

		private T getData() {
			return data;
		}

		private void setData(T newData) {
			data = newData;
		}

		private Node getNextNode() {
			return next;
		}

		private void setNextNode(Node nextNode) {
			next = nextNode;
		}
	}
}
